<?php

namespace Drupal\onlyone\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class OnlyOneEvents.
 *
 * Defines the events related to the OnlyOne module.
 */
class OnlyOneEvents extends Event {

  /**
   * Event related to a change in the content types configuration.
   */
  const CONTENT_TYPES_UPDATED = 'onlyone.content_types_updated';

}
